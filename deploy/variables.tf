variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipiente-app-api-devops"
}

variable "contact" {
  default = "jean-9116@hotmail.com"
}
